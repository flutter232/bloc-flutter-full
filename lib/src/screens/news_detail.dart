import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../blocs/comments_provider.dart';
import '../models/item_models.dart';
import '../widgets/comment.dart';

class NewsDetail extends StatelessWidget {
  final int itemId;
  const NewsDetail({Key? key, required this.itemId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final commentsBloc = CommentsProvider.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('News Detail'),
      ),
      body: buildBody(commentsBloc),
    );
  }

  Widget buildBody(CommentsBloc bloc) {
    return StreamBuilder(
      builder: (BuildContext context,
          AsyncSnapshot<Map<int, Future<ItemModel?>>> snapshot) {
        if (!snapshot.hasData) {
          return const Text("Loading");
        }
        final itemFuture = snapshot.data?[itemId];
        return FutureBuilder(
          future: itemFuture,
          builder:
              (BuildContext context, AsyncSnapshot<ItemModel?> itemSnapshot) {
            if (!itemSnapshot.hasData) {
              return const Text("Loading");
            }
            return buildList(itemSnapshot.data, snapshot.data);
          },
        );
      },
      stream: bloc.itemWithComments,
    );
  }

  Widget buildList(ItemModel? item, Map<int, Future<ItemModel?>>? itemMap) {
    final children = <Widget>[];
    children.add(buildTitle(item));
    final commentsList = (item?.kids ?? []).map((kidId) {
      return Comment(
        itemId: kidId,
        itemMap: itemMap,
      );
    }).toList();
    children.addAll(commentsList);
    return ListView(children: children);
  }

  Widget buildTitle(ItemModel? itemModel) {
    return Container(
      margin: const EdgeInsets.all(10),
      alignment: Alignment.topCenter,
      child: Text(
        itemModel?.title ?? itemModel?.text ?? "No data",
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
