import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:news/src/widgets/refresh.dart';
import '../blocs/stories_provider.dart';
import '../widgets/news_list_tile.dart';

class NewsList extends StatelessWidget {
  const NewsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = StoriesProvider.of(context);
    bloc.fetchTopIds();
    return Scaffold(
        appBar: AppBar(
          title: const Text('News List'),
        ),
        body: buildList(bloc));
  }

  Widget buildList(StoriesBloc bloc) {
    return StreamBuilder(
      stream: bloc.topIds,
      builder: (BuildContext context, AsyncSnapshot<List<int>> snapshot) {
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return Refresh(
            child: ListView.builder(
                itemCount: (snapshot.data as List<int>).length,
                itemBuilder: (BuildContext context, int index) {
                  bloc.fetchItem(snapshot.data![index]);
                  if (kDebugMode) {
                    print(index);
                  }
                  return NewsListTile(itemId: snapshot.data![index]);
                }));
      },
    );
  }
}
