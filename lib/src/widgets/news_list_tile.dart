import 'package:flutter/material.dart';
import 'package:news/src/widgets/loading_container.dart';
import '../models/item_models.dart';
import '../blocs/stories_provider.dart';

class NewsListTile extends StatelessWidget {
  final int itemId;
  const NewsListTile({Key? key, required this.itemId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final StoriesBloc bloc = StoriesProvider.of(context);
    return StreamBuilder(
      builder: (context, AsyncSnapshot<Map<int, Future<ItemModel?>>> snapshot) {
        if (!snapshot.hasData) {
          return const Center(
            child: LoadingContainer(),
          );
        }
        return FutureBuilder(
          future: snapshot.data?[itemId],
          builder: (context, AsyncSnapshot<ItemModel?> itemSnapshot) {
            if (!itemSnapshot.hasData) {
              return const Center(
                child: LoadingContainer(),
              );
            }
            return buildTile(context, (itemSnapshot.data as ItemModel));
          },
        );
      },
      stream: bloc.items,
    );
  }

  Widget buildTile(BuildContext context, ItemModel item) {
    return Column(
      children: [
        ListTile(
          title: Text(item.title ?? item.text ?? ''),
          onTap: () {
            Navigator.pushNamed(context, '/${item.id}');
          },
          subtitle: Text("${item.score} points"),
          trailing: buildComments(item),
        ),
        const Divider(
          height: 8.0,
        ),
      ],
    );
  }

  Widget buildComments(ItemModel item) {
    return Column(
      children: [
        const Icon(Icons.comment),
        Text("${item.descendants}"),
      ],
    );
  }
}
