import 'package:flutter/material.dart';
import 'package:news/src/models/item_models.dart';

class Comment extends StatelessWidget {
  final int itemId;
  final Map<int, Future<ItemModel?>>? itemMap;
  const Comment({Key? key, required this.itemId, required this.itemMap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        builder: (BuildContext context, AsyncSnapshot<ItemModel?> snapshot) {
          if (!snapshot.hasData) {
            return const Text('Still loading Comment');
          }
          final ItemModel? item = snapshot.data;
          final children = <Widget>[
            ListTile(
              title: Text(item?.text ?? item?.title ?? 'No Comment'),
              subtitle: Text(item?.by ?? 'No Author'),
            ),
            const Divider(),
          ];
          for (var kidId in (item?.kids ?? [])) {
            children.add(Comment(
              itemId: kidId,
              itemMap: itemMap,
            ));
          }
          return Column(
            children: children,
          );
        },
        future: itemMap?[itemId]);
  }
}
