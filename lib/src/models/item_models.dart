class ItemModel {
  late final int id;
  late final String type;
  late final String? by;
  late final int? time;
  late final String? text;
  late final String? title;
  late final int? score;
  late final List<int>? kids;
  late final int? descendants;

  ItemModel(
      {required this.id,
      required this.type,
      this.by,
      this.time,
      this.title,
      this.text,
      this.score,
      this.kids,
      this.descendants});

  ItemModel.fromJson(Map<String, dynamic> payload)
      : id = payload['id'],
        title = payload['title'] ?? '',
        type = payload['type'],
        by = payload['by'] ?? '',
        time = payload['time'],
        text = payload['text'] ?? '',
        score = payload['score'] ?? 0,
        kids = (payload['kids'] ?? []).cast<int>(),
        descendants = payload['descendants'] ?? 0;

  ItemModel.fromDb(Map<String, dynamic> payload)
      : id = payload['id'],
        type = payload['type'],
        by = payload['by'] ?? '',
        time = payload['time'],
        text = payload['text'] ?? '',
        title = payload['title'] ?? '',
        score = payload['score'] ?? 0,
        kids = payload['kids'] ?? [],
        descendants = payload['descendants'] ?? 0;

  Map<String, Object?> toMap() => <String, Object?>{
        "id": id,
        "type": type,
        "by": by,
        "time": time,
        "text": text,
        "title": title,
        "score": score,
        "kids": kids,
        "descendants": descendants
      };
}
