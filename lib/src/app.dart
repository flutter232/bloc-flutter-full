import 'package:flutter/material.dart';
import './blocs/stories_provider.dart';
import './blocs/comments_provider.dart';
import './screens/news_list.dart';
import './screens/news_detail.dart';

class NewsApp extends StatelessWidget {
  const NewsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CommentsProvider(
        child: StoriesProvider(
            child: MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News App',
      onGenerateRoute: routes,
    )));
  }

  Route<dynamic> routes(RouteSettings settings) {
    if (settings.name == '/') {
      return MaterialPageRoute(
        builder: (BuildContext context) {
          return const NewsList();
        },
        settings: settings,
      );
    } else {
      return MaterialPageRoute(
        builder: (BuildContext context) {
          final int itemId = int.parse(settings.name!.replaceFirst("/", ""));
          final commentsBloc = CommentsProvider.of(context);
          commentsBloc.fetchItemWithComments(itemId);
          return NewsDetail(itemId: itemId);
        },
        settings: settings,
      );
    }
  }
}
