import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'dart:io';
import '../models/item_models.dart';
import 'repository.dart';

class NewsDbProvider implements Source, Cache {
  late Database db;

  NewsDbProvider() {
    init();
  }

  init() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, "news_4.db");
    db = await openDatabase(path, version: 1,
        onCreate: (Database newDb, int version) async {
      await newDb.execute("""
        CREATE TABLE IF NOT EXISTS Items
          (
            id INTEGER PRIMARY KEY,
            type TEXT,
            title TEXT,
            by TEXT,
            text TEXT,
            time INTEGER,
            score INTEGER,
            kids BLOB,
            descendants INTEGER
          );""");
    });
  }

  @override
  Future<ItemModel?> fetchItem(int id) async {
    final maps = await db.query("Items", where: "id = ?", whereArgs: [id]);
    if (maps.isNotEmpty) {
      return ItemModel.fromDb(maps.first);
    }
    return null;
  }

  @override
  Future<int> addItem(ItemModel item) {
    return db.insert("Items", item.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  @override
  Future<List<int>> fetchTopIds() async {
    final data = await db.query("Items", columns: ["id"]);
    return data.map<dynamic>((item) => item["id"]).toList().cast<int>();
  }

  @override
  Future<int> clear() {
    return db.delete("Items");
  }
}

final newsDbProvider = NewsDbProvider();
