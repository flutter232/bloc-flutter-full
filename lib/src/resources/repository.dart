import 'package:flutter/foundation.dart';
import 'dart:async';
import 'news_api_provider.dart' show newsApiProvider;
import 'news_db_provider.dart' show newsDbProvider;
import '../models/item_models.dart';

class Repository {
  List<Source> sources = <Source>[newsDbProvider, newsApiProvider];
  List<Cache> caches = <Cache>[newsDbProvider];

  Future<List<int>> fetchTopIds() async {
    return sources[1].fetchTopIds();
  }

  Future<ItemModel?> fetchItem(int id) async {
    late ItemModel? item;
    late Source sourceTemp;
    for (var source in sources) {
      item = await source.fetchItem(id);
      if (item != null) {
        sourceTemp = source;
        break;
      }
    }
    for (var cache in caches) {
      if ((cache as Source) != sourceTemp && kIsWeb) {
        if (item != null) {
          cache.addItem(item);
        }
      }
    }
    return item;
  }

  clearCache() async {
    for (var cache in caches) {
      await cache.clear();
    }
  }
}

abstract class Source {
  Future<List<int>> fetchTopIds();
  Future<ItemModel?> fetchItem(int id);
}

abstract class Cache {
  Future<int> addItem(ItemModel item);
  Future<int> clear();
}
