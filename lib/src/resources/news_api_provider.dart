import 'dart:convert';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client;
import '../models/item_models.dart';
import 'repository.dart';

class NewsApiProvider implements Source {
  Client client = Client();
  Uri _buildUrl(String endpoint) {
    return Uri.https("hacker-news.firebaseio.com", "v0/$endpoint.json");
  }

  @override
  Future<List<int>> fetchTopIds() async {
    final response = await client.get(_buildUrl("topstories"));
    final List<dynamic> ids = json.decode(response.body);
    return ids.cast<int>();
  }

  @override
  Future<ItemModel?> fetchItem(int id) async {
    final response = await client.get(_buildUrl("item/$id"));
    if (response.statusCode == 200) {
      final parsedJson = json.decode(response.body);
      return ItemModel.fromJson(parsedJson);
    }
    return null;
  }
}

final newsApiProvider = NewsApiProvider();
