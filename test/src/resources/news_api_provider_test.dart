import 'package:news/src/resources/news_api_provider.dart';
import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';

void main() {
  test('FetchTopIds returns a list of ids', () async {
    final newsApi = NewsApiProvider();
    newsApi.client = MockClient((request) async {
      return Response(json.encode([1, 2, 3, 4]), 200);
    });
    final ids = await newsApi.fetchTopIds();
    expect(ids, [1, 2, 3, 4]);
  });

  test('FetchItem returns a item model', () async {
    final newsApi = NewsApiProvider();
    newsApi.client = MockClient((request) async {
      final jsonMap = {
        "by": "pg",
        "id": 160705,
        "poll": 160704,
        "score": 335,
        "text":
            "Yes, ban them; I'm tired of seeing Valleywag stories on News.YC.",
        "time": 1207886576,
        "type": "pollopt"
      };
      return Response(json.encode(jsonMap), 200);
    });

    final item = await newsApi.fetchItem(160705);

    expect(item?.id, 160705);
  });
}
